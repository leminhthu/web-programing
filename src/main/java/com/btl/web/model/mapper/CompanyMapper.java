package com.btl.web.model.mapper;

import com.btl.web.model.Company;
import com.btl.web.model.dto.CompanyDTO;
import org.springframework.stereotype.Component;

@Component
public class CompanyMapper {

    public CompanyDTO convertToDTO(Company company){
        return CompanyDTO.builder()
                .companyName(company.getCompanyName())
                .authorizedCapital(company.getAuthorizedCapital())
                .fieldOfOperation(company.getFieldOfOperation())
                .addressInBuilding(company.getAddressInBuilding())
                .groundArea(company.getGroundArea())
                .phoneNumber(company.getPhoneNumber())
                .taxNumber(company.getTaxNumber())
                .build();
    }

    public Company convertToEntity(CompanyDTO companyDTO){
        return Company.builder()
                .companyName(companyDTO.getCompanyName())
                .addressInBuilding(companyDTO.getAddressInBuilding())
                .authorizedCapital(companyDTO.getAuthorizedCapital())
                .fieldOfOperation(companyDTO.getFieldOfOperation())
                .phoneNumber(companyDTO.getPhoneNumber())
                .taxNumber(companyDTO.getTaxNumber())
                .groundArea(companyDTO.getGroundArea())
                .build();
    }
}
